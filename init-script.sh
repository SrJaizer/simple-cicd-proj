#!/bin/bash
# Bastion machine manual script, execute only the first time and before cicd-script.sh

  docker build -t srjaizer/proj2-database:1.0 database/.
  docker push srjaizer/proj2-database:1.0

# Execute resources from hidden files:

  kubectl apply -f ./load-balancer.yml
  kubectl apply -f ./secrets.yml